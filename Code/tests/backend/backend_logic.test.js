require('jest-fetch-mock').enableMocks();
const backend_logic = require('../../src/components/backend/backend_logic.js');
const config = require('../../src/components/backend/config.js');

jest.useFakeTimers();
jest.spyOn(global, 'setTimeout');

test("Test Free User", () =>{
    let array3 = new Array(3).fill(false);
    array3[1] = true;
    expect(backend_logic.freeUser(array3)).toBe(0);
    array3[0] = true;
    expect(backend_logic.freeUser(array3)).toBe(2);
    array3[2] = true;
    expect(backend_logic.freeUser(array3)).toBe(-1);
})

describe('testing init', () => {
    beforeEach(() => {
        fetch.resetMocks()
    })
    
    test("Test Init - Function", () =>{
        backend_logic.UserColours = ['123123', '123123', '123123']
        backend_logic.activeUsers = new Array(3).fill(true);
        expect(backend_logic.UserColours[0]).not.toStrictEqual(config.UserColors.User1);
        expect(backend_logic.activeUsers).not.toStrictEqual([false, false, false]);
        fetch.mockResponseOnce(JSON.stringify({ data: '12345' }))

        //backend_logic.init();
        //console.log(backend_logic.UserColours);
        // expect(backend_logic.UserColours[0]).toStrictEqual(config.UserColors.User1);
        // expect(backend_logic.activeUsers).toStrictEqual([false, false, false]);
    })

})

describe('testing postJSON', () => {
    beforeEach(() => {
      fetch.resetMocks()
    })
   
    test('check postJSON', () => {
      fetch.mockResponseOnce(JSON.stringify({ data: '12345' }))
   
      //assert on the response
      backend_logic.postJSON('Zeki-Network', 'hello').then(res => {

        expect(res.data).toEqual('12345')
      })
   
        //assert on the times called and arguments given to fetch
        expect(fetch.mock.calls.length).toEqual(1)
        expect(fetch.mock.calls[0][0]).toEqual('Zeki-Network')
        expect(fetch.mock.calls.length).not.toEqual(0)
        expect(fetch.mock.calls[0][0]).not.toEqual('Wegeleitsystem')
    })
  })

test("TEST sleep", async () => {

    backend_logic.sleep(1);
})


describe('testing Timer', () => {
    beforeEach(() => {
      fetch.resetMocks()
    })
    test("TEST timer", async() => {
        fetch.mockResponseOnce(JSON.stringify())

        config.sleepTime = 1;
        backend_logic.timer(config.ESPs.FocusSpace, config.JSONs.FocusSpace, 0);

    })
})



// describe('testing active Path to Room', () => {
//     beforeEach(() => {
//       fetch.resetMocks()
//     })

//     test("TEST activate Path to Room", async() => {
//         const roomName = "ExperienceHub";
//         fetch.mockResponseOnce();

//         backend_logic.activeUsers = new Array(3).fill(false);
//         await backend_logic.activatePathToRoom(roomName);
//         console.log(backend_logic.activeUsers);

//     })
// })

// test("TEST send User ID to Frontend", () => {

// })