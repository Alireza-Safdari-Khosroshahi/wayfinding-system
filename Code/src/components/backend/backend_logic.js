/*******************************************************************************
* Distributed Artificial Intelligence Laboratory TU Berlin.
* Wayfinding system (BACKEND TEAM)
* All Rights Reserved.
********************************************************************************
* Filename    : backend_logic.js
* Description : This file contains the logic of the backend.
* please do not change if you do not understand something.
*
* History
*-------------------------------------------------------------------------------
* Date                       Name                    Description of Change
* 15.11.2022                 Alireza                 create file
* 1.12.2022                  Yanic                   add timer fuction
* 17.12.2022                 Alireza                 Add postJSON and room functions
* 02.01.2023                 Yanic                   add/edit JsonOff function
* 23.1.2023                  Alireza                 add init function
* 08.02.2023                 Mier                    add MultiUser functionality
* 10.02.2023                 Alireza/Tobi            Optimization of multi-user functionality
* 15.02.2023                 Adrian Brag             added "send to Frontend data" feature
*-------------------------------------------------------------------------------
* Developers :  Alireza Safdari (@mhsafdari)
                Yannic Stoer (@yannicstoer)
                Tobias Schulz (@tobi-study)
                Mier Barsanjy (@mier.barsanjy)
*******************************************************************************/

// import config from '../../../../Backend/src/config'

/* eslint-disable */
import config from './config'

export let UserColours = ['e1e0a0', 'FF0000', '00FF00'];
export let activeUsers = new Array(3).fill(false);
let currentUsers = 0;

export function freeUser(inArr) {
    for (let i = 0; i < inArr.length; i++){
        if (inArr[i]==false){
            return i;
        }
    }
    return -1;
}

export function init() {
    UserColours[0] = config.UserColors.User1;
    UserColours[1] = config.UserColors.User2;
    UserColours[2] = config.UserColors.User3;
    //this.activeUsers.fill(false);
    let Esps = config.init.initESPs;
    let init_json_main = config.JSONs.Initial[0];
    let init_json = config.JSONs.Initial[1];
    console.log("adress: " + Esps);
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(init_json_main),
    };
    fetch(Esps[0], requestOptions)
        .then((response) => response.json())
    for (let i = 1; i < Esps.length; i++) {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(init_json),
        };
        fetch(Esps[i], requestOptions)
            .then((response) => response.json())
    }
}
export async function postJSON(addr, json) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(json),
    };
    return fetch(addr, requestOptions)
        .then((response) =>  response.json())
    //console.log("sended json: ", json);
}
export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms * 1000));
}

// for test reasons made localCurrentUser optional
export async function timer(Esps, JSON, localCurrentUser = 1) {
    // let localCurrentUser = freeUser(activeUsers)
    // let localCurrentUser = currentUsers - 1;

    await sleep(config.sleepTime);
    for (let j = 0; j < JSON.length; j++) {
        for (let l = 0; l < JSON[j].seg.length; l++)
            JSON[j].seg[l].col[localCurrentUser] = 0x000000
    }
    for (let i = 0; i < Esps.length; i++) {
        postJSON(Esps[i], JSON[i]);
    }
    // deactivate User so it can be used again
    activeUsers[localCurrentUser] = false;
    console.log("Path deactivated.");
    console.log("localCurrentUser: " + localCurrentUser);
    console.log("Active User: " + activeUsers);
    sendUserIDtoFrontend(-1, false);

    if (currentUsers > 0) {
        // currentUsers--;
        // decrementUsers();
        console.log("User was reduced " + currentUsers);
    }
}

// dynamic room approach
export function activatePathToRoom(roomName) {
    let userID = freeUser(this.activeUsers);
    activeUsers[userID] = true;
    if (userID != -1){
    // if (currentUsers < 4) {
        let Esps = config.ESPs[roomName];
        let JSON = config.JSONs[roomName];
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID]
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i]);
        }
      //   //incrementUsers();
        timer(Esps, JSON, userID);
    // }
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}


export function ExperienceHub() {
    let userID = freeUser(activeUsers);
    activeUsers[userID] = true;
    console.log("Experience Hub activated")
    console.log(userID);
    console.log(activeUsers);
    if (userID != -1){
    // if (currentUsers < 4) {
        let Esps = config.ESPs.ExperienceHub;
        let JSON = config.JSONs.ExperienceHub;
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID];
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i]);
        }

        // //incrementUsers();
        timer(Esps, JSON, userID);
    // }
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}
export function Konferenzraum() {
    let userID = freeUser(activeUsers);
    activeUsers[userID] = true;
    console.log("Konferenzraum activated")
    console.log(userID);
    console.log(activeUsers);
    if (userID != -1) {
        let Esps = config.ESPs.Konferenzraum;
        let JSON = config.JSONs.Konferenzraum;
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID];
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i]);

        }

        // //incrementUsers();
        timer(Esps, JSON, userID);
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}
export function ManagementBeuro() {
    let userID = freeUser(activeUsers);
    activeUsers[userID] = true;
    console.log("ManagementBeuro activated")
    console.log(userID);
    console.log(activeUsers);
    if (userID != -1) {
        let Esps = config.ESPs.ManagementBeuro;
        let JSON = config.JSONs.ManagementBeuro;
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID]
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i]);
        }

        // //incrementUsers();
        timer(Esps, JSON, userID);
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}
export function FocusSpace() {
    let userID = freeUser(activeUsers);
    activeUsers[userID] = true;
    console.log("Focus Space activated")
    console.log(userID);
    console.log(activeUsers);
    if (userID != -1) {
        let Esps = config.ESPs.FocusSpace;
        let JSON = config.JSONs.FocusSpace;
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID]
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i], Esps);
        }

        // //incrementUsers();
        timer(Esps, JSON, userID);
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}
export function DesignThinkingSpace() {
    let userID = freeUser(activeUsers);
    activeUsers[userID] = true;
    console.log("DesignThinkingSpace activated")
    console.log(userID);
    console.log(activeUsers);
    if (userID != -1) {
        let Esps = config.ESPs.DesignThinkingSpace;
        let JSON = config.JSONs.DesignThinkingSpace;
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID]
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i]);
        }

        // //incrementUsers();
        timer(Esps, JSON, userID);
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}
export function CoWorkingSpace() {
    let userID = freeUser(activeUsers);
    activeUsers[userID] = true;
    console.log("CoWorkingSpace activated")
    console.log(userID);
    console.log(activeUsers);
    if (userID != -1) {
        let Esps = config.ESPs.CoWorkingSpace;
        let JSON = config.JSONs.CoWorkingSpace;
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID]
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i]);
        }

        // //incrementUsers();
        timer(Esps, JSON, userID);
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}
export function LieferroboterTeleoperator() {
    let userID = freeUser(activeUsers);
    activeUsers[userID] = true;
    console.log("LieferroboterTeleoperator activated")
    console.log(userID);
    console.log(activeUsers);
    if (userID != -1) {
        let Esps = config.ESPs.LieferroboterTeleoperator;
        let JSON = config.JSONs.LieferroboterTeleoperator;
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID]
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i]);
        }

        // //incrementUsers();
        timer(Esps, JSON, userID);
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}
export function Lieferroboterraum() {
    let userID = freeUser(activeUsers);
    activeUsers[userID] = true;
    console.log("LieferroboterTeleoperator activated")
    console.log(userID);
    console.log(activeUsers);
    if (userID != -1) {
        let Esps = config.ESPs.Lieferroboterraum;
        let JSON = config.JSONs.Lieferroboterraum;
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID]
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i]);
        }

        // //incrementUsers();
        timer(Esps, JSON, userID);
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}
export function WCHerren() {
    let userID = freeUser(activeUsers);
    activeUsers[userID] = true;
    console.log("LieferroboterTeleoperator activated")
    console.log(userID);
    console.log(activeUsers);
    if (userID != -1) {
        let Esps = config.ESPs.WCHerren;
        let JSON = config.JSONs.WCHerren;
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID]
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i]);
        }

        // //incrementUsers();
        timer(Esps, JSON, userID);
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}
export function WCFrauen() {
    let userID = freeUser(activeUsers);
    activeUsers[userID] = true;
    console.log("LieferroboterTeleoperator activated")
    console.log(userID);
    console.log(activeUsers);
    if (userID != -1) {
        let Esps = config.ESPs.WCFrauen;
        let JSON = config.JSONs.WCFrauen;
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID]
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i]);
        }

        //incrementUsers();
        timer(Esps, JSON,userID);
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}
export function Kueche() {
    let userID = freeUser(activeUsers);
    activeUsers[userID] = true;
    console.log("LieferroboterTeleoperator activated")
    console.log(userID);
    console.log(activeUsers);
    if (userID != -1) {
        let Esps = config.ESPs.Küche;
        let JSON = config.JSONs.Küche;
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID]
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i]);
        }

        //incrementUsers();
        timer(Esps, JSON, userID);
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}
export function Technikraum() {
    let userID = freeUser(activeUsers);
    activeUsers[userID] = true;
    console.log("LieferroboterTeleoperator activated")
    console.log(userID);
    console.log(activeUsers);
    if (userID != -1) {
        let Esps = config.ESPs.Technikraum;
        let JSON = config.JSONs.Technikraum;
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID]
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i]);
        }

        //incrementUsers();
        timer(Esps, JSON);
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}
export function WCUnisex() {
    let userID = freeUser(activeUsers);
    activeUsers[userID] = true;
    console.log("LieferroboterTeleoperator activated")
    console.log(userID);
    console.log(activeUsers);
    if (userID != -1) {
        let Esps = config.ESPs.WCUnisex;
        let JSON = config.JSONs.WCUnisex;
        for (let j = 0; j < JSON.length; j++) {
            for (let l = 0; l < JSON[j].seg.length; l++)
                JSON[j].seg[l].col[userID] = UserColours[userID]
        }
        for (let i = 0; i < Esps.length; i++) {
            postJSON(Esps[i], JSON[i]);
        }

        //incrementUsers();
        timer(Esps, JSON,userID);
    }
    sendUserIDtoFrontend(userID, freeUser(activeUsers) == -1);

}

/* --------------------------------Frontend emit for currentUsers --------------------------------*/
//~Adrian

import mitt from 'mitt'

export { currentUsers };
const bus = mitt();

// sends ID 0,1,2 or -1, if no slot is available.
function sendUserIDtoFrontend(userID, blockBoolean){
    bus.emit('currentUsersChange', userID);
    bus.emit('userCapacity', blockBoolean);
}

// function incrementUsers() {
//     currentUsers ++;
//     bus.emit('currentUsersChange', currentUsers);
// }
// function decrementUsers() {
//     currentUsers --;
//     bus.emit('currentUsersChange', currentUsers);
// }

export { bus };


/* -----------------------------------------------------------------------------------------------*/
