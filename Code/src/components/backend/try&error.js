/*******************************************************************************
* Distributed Artificial Intelligence Laboratory TU Berlin.
* Wayfinding system (BACKEND TEAM)
* All Rights Reserved.
********************************************************************************
* Filename    : try&error.js
* Description : this file is just for testing our functions
*
* History
*-------------------------------------------------------------------------------
* Date                       Name                    Description of Change
*
*
*-------------------------------------------------------------------------------
* Developers :  Alireza Safdari (@mhsafdari)
                Yannic Stoer (@yannicstoer)
                Tobias Schulz (@tobi-study)
                Mier Barsanjy (@mier.barsanjy)
*******************************************************************************/


function chooseColor(numberOfUsers) {
        if (numberOfUsers===1){
            this.color = '[[255,0,0],[0,0,0],[0,0,0]]';
        }
        if (numberOfUsers===2){
            this.color = '[[255,0,0],[0,255,0],[0,0,0]]';
        }
        if (numberOfUsers===3){
            this.color = '[[255,0,0],[0,255,0],[0,0,255]]';
        }
        console.log(this.color);
}



// generate random rgb color
function randomRGB() {
        let R = Math.floor(Math.random() * 256);
        let G = Math.floor(Math.random() * 256);
        let B = Math.floor(Math.random() * 256);
        let RGBColor = "[" + R + "," + G + "," + B + "]";
        return RGBColor;
}

// calculate time until light is turned off again. Default: 5s
function calculateTime(segmentsCount = 50){
    /*
        3,5km/h = ~1m/s
        1 segment = 10cm
        -> pro Segment 0,1s Licht
        */

        // return value in ms, because setTimeout takes ms as input
            return(segmentsCount*100);
    }

function freeUser(inArr){
        for (let i = 0; i < inArr.length; i++){
                if (inArr[i]==false){
                return i;
            }
        }
        return -1;
}
function lightBand(){
    let nextUserId = this.freeUser(config.activeUsers.actusr);
    config.activeUsers.actusr[nextUserId] = true;
    console.log(config.activeUsers.actusr);
    //let color = userColor(nextUserId);

    //let json = buildJSON(color);

    // call function with JSON
    // this.on();

    // calculate time until turn off
    let time = this.calculateTime();
    // call function to turn off - also needs to set user to false again
    setTimeout(function(){
        config.activeUsers.actusr[nextUserId]=false;
        console.log(config.activeUsers.actusr);
        // off();
        }, time);
}

// const response = await fetch("http://4.3.2.1/json", {
// method: 'POST',
// headers: {
//   'Accept': 'application/json',
//   'Content-Type': 'application/json'
// },
// body: `{ "on":true,"bri":128,"transition":7,"mainseg":0,"seg":[{"id":0,"start":0,"stop":180,"grp":1,"spc":0,"of":0,"on":true,"frz":false,"bri":255,"cct":127,"col":[[0,0,0],[0,0,0],[0,0,0]],"fx":52,"sx":128,"ix":128,"pal":0,"sel":true,"rev":false,"mi":false},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0}]}`,
// });

// response.json().then(data => {
//   console.log(data);
// });
// Example POST method implementation:

'use strict'
const fetch = require('node-fetch')
async function postData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
  }
  postData('http://wled-005/json/state', {"on":true,"bri":128,"transition":7,"mainseg":0,"seg":[{"id":0,"start":0,"stop":180,"grp":1,"spc":0,"of":0,"on":true,"frz":false,"bri":255,"cct":127,"col":[[0,0,0],[0,0,0],[0,0,0]],"fx":52,"sx":128,"ix":128,"pal":0,"sel":true,"rev":false,"mi":false},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0}]})
    .then((data) => {
    console.log(data); // JSON data parsed by `data.json()` call
    });

// const response = await fetch("http://4.3.2.1/json", {
// method: 'POST',
// headers: {
//   'Accept': 'application/json',
//   'Content-Type': 'application/json'
// },
// body: `{ "on":true,"bri":128,"transition":7,"mainseg":0,"seg":[{"id":0,"start":0,"stop":180,"grp":1,"spc":0,"of":0,"on":true,"frz":false,"bri":255,"cct":127,"col":[[0,0,0],[0,0,0],[0,0,0]],"fx":52,"sx":128,"ix":128,"pal":0,"sel":true,"rev":false,"mi":false},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0},{"stop":0}]}`,
// });

// response.json().then(data => {
//   console.log(data);
// });
// Example POST method implementation:

'use strict'
const fetch = require('node-fetch')
async function postData(url = '', data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

postData('http://4.3.2.1/json/state', { "on": true, "bri": 128, "transition": 7, "mainseg": 0, "seg": [{ "id": 0, "start": 0, "stop": 180, "grp": 1, "spc": 0, "of": 0, "on": true, "frz": false, "bri": 255, "cct": 127, "col": [[255, 255, 255], [0, 0, 0], [0, 0, 0]], "fx": 52, "sx": 128, "ix": 128, "pal": 0, "sel": true, "rev": false, "mi": false }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }, { "stop": 0 }] })
  .then((data) => {
    console.log(data); // JSON data parsed by `data.json()` call
  });