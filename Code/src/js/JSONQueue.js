var JSONQueue = {
    queue: [],
    constructor() {
        this.queue = [];
    },

    add(json) {
        this.queue.push(json);
    },

    processNext() {
        if (this.queue.length > 0) {
            let json = this.queue.shift();

            // To Do: Send it to the Controller
            //POST JSON
            //console.log(json);
            return json;
        }
        else{
            console.log("No JSON to send");
        }
    },
    getQueue(){
        return this.queue;
    },

    isEmpty() {
        return this.queue.length === 0;
      },
}

export default JSONQueue;

